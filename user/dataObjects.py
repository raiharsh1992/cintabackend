##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
###############DATA CLASS OBJECT HOLDER LAYER#################
##############################################################
##############################################################

class loginReuest:
    def __init__(self, userName, password):
        self.userName = userName
        self.password = password

class sessionInfo:
    def __init__(self, sessionId, basicKey, sessionKey, createdOn, userId, isActive):
        self.sessionId = sessionId
        self.basicKey = basicKey
        self.sessionKey = sessionKey
        self.createdOn = createdOn
        self.userId = userId
        self.isActive = isActive

class deviceInfo:
    def __init__(self, deviceKey, email, deviceName, deviceType):
        self.deviceKey = deviceKey
        self.email = email
        self.deviceName = deviceName
        self.deviceType = deviceType
