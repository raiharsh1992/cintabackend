from django.http import JsonResponse
import json
import logging
from datetime import datetime
# Create your views here.

from .interaction import loginUser, registerNewDevice, insertingNewValues, viewAllTheDevices, viewAllTheTrackingInfo, logOutUser

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/view.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
#################REQUEST VIEWING HOLDER LAYER#################
##############################################################
##############################################################
##############################################################

#Login Api Interaction Point Begins
def login(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Login in for'+str(request.body))
                response = loginUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Register a new device Api Interaction Point Begins
def registerdevice(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Registering Device for'+str(request.body))
                response = registerNewDevice(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Insert tracking information Api Interaction Point Begins
def insertvalues(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Inserting new tracking information'+str(request.body))
                response = insertingNewValues(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing list of all the devices Api Interaction Point Begins
def viewdevices(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing all the registered devices'+str(request.body))
                response = viewAllTheDevices(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing list of all the tracking data of a device Api Interaction Point Begins
def viewtracking(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing all the registered devices'+str(request.body))
                response = viewAllTheTrackingInfo(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Logging out admin user Api Interaction Point Begins
def logout(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing all the registered devices'+str(request.body))
                response = logOutUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
