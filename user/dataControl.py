# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import JsonResponse
import uuid
from datetime import datetime, date

from .dataHandler import validSessionBasicAut, validSessionSessionKey, getUserIdFromLoginInfo, userDeviceList, userDecviceTracking

from .dataObjects import sessionInfo

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA CONTROL HOLDER LAYER###################
##############################################################
##############################################################
##############################################################

def getSessionInfoObject(loginRequest):
    userId = getUserIdFromLoginInfo(loginRequest)
    if userId ==0:
        return userId
    else:
        basicKey = uuid.uuid4().hex
        if validSessionBasicAut(basicKey)!=0:
            while validSessionBasicAut(basicKey)!=0:
                basicKey = uuid.uuid4().hex
        sessionKey = uuid.uuid4().hex
        if validSessionSessionKey(sessionKey)!=0:
            while validSessionSessionKey(sessionKey)!=0:
                sessionKey = uuid.uuid4().hex
        createdOnDate =  datetime.now()
        sessionInformation = sessionInfo(0, basicKey, sessionKey, createdOnDate, userId, 1)
        return sessionInformation

def getAllTheDevicesList():
    deviceList = userDeviceList()
    container = []
    for each in deviceList:
        container.append({
            'deviceId':each[0],
            'emailAddress':each[1],
            'deviceIdentifier':each[2],
            'deviceType':each[3],
            'deviceName':each[4]
        })
    response = JsonResponse({"Data":container})
    response.status_code = 200
    return response

def getAllTheTrackingDetails(deviceId):
    trackingList = userDecviceTracking(deviceId)
    container = []
    for each in trackingList:
        container.append({
            'trackId':each[0],
            'deviceId':each[1],
            'tackingValue':each[2],
            'DateTimeField':each[3]
        })
    response = JsonResponse({"Data":container})
    response.status_code = 200
    return response
