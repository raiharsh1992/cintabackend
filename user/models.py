from django.db import models

# Create your models here.
class loginMaster(models.Model):
    userId = models.AutoField(primary_key=True)
    userName = models.CharField(unique=True, blank=False, null=False, max_length=64)
    password = models.CharField(unique=False, blank=False, null=False, max_length=32)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class sessionMaster(models.Model):
    sessionId = models.AutoField(primary_key=True)
    basicAuthenticate = models.CharField(max_length=128, blank=False, null=False, unique=True)
    sessionInfo = models.CharField(max_length=128, blank=False, null=False, unique=True)
    createdOn = models.DateTimeField()
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class deviceMaster(models.Model):
    deviceId = models.AutoField(primary_key=True)
    emailAddress = models.TextField(blank=False, null=False, unique=False)
    deviceIdentifier = models.CharField(blank=False, null=False, unique=False, max_length=255)
    deviceType = models.CharField(blank=False, null=False, unique=False, max_length=255)
    deviceName = models.CharField(blank=False, null=False, unique=False, max_length=255)
    def __str__(self):
        return self.name

class trackingMaster(models.Model):
    trackId = models.AutoField(primary_key=True)
    deviceId = models.ForeignKey(deviceMaster, on_delete=models.PROTECT)
    tackingValue = models.CharField(blank=False, null=False, unique=False, max_length=50)
    DateTimeField = models.DateTimeField()
    def __str__(self):
        return self.name
