# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from datetime import datetime, date, timedelta
from django.http import JsonResponse
import logging

from .dataControl import getSessionInfoObject, getAllTheDevicesList, getAllTheTrackingDetails

from .dataHandler import isValidLoginRequest, insertSessionInformation, isDeviceAlreadyRegistere, createNewDeviceInfo, insertNewTrackingValue, isValidSessionInformation, isValidDevice, loggingOutSession

from .dataObjects import loginReuest, deviceInfo, sessionInfo

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/interaction.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
####################DATA WORK HOLDER LAYER####################
##############################################################
##############################################################
##############################################################

# Manages the login request for validating if the values are correct and then return the session values
def loginUser(request):
    try:
        #reading Json request
        rString = json.loads(request.body)
        loginReuestObject = loginReuest(rString['userName'], rString['password'])
        #Check if valid username password and usertype exists in the system
        if isValidLoginRequest(loginReuestObject):
            #Create a session object with the provided login details
            sessionInfoObject = getSessionInfoObject(loginReuestObject)
            #Check if the created session object has values or not
            if sessionInfoObject==0:
                response = JsonResponse({"Data":"There is some inconsitency in data processing"})
                response.status_code = 409
                return response
            else:
                #Insert session information in the database, and if success return session info or else return not able to process information in this moment
                if insertSessionInformation(sessionInfoObject):
                    response = JsonResponse({"Data":"Login successful","sessionInfo":{"basicAuth":sessionInfoObject.basicKey, "sessionKey":sessionInfoObject.sessionKey,"userId":sessionInfoObject.userId}})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Not able to login in this moment please try again later"})
                    response.status_code = 409
                    return response
        else:
            response = JsonResponse({"Data":"Invalid login request passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def registerNewDevice(request):
    try:
        rString = json.loads(request.body)
        deviceData = deviceInfo(rString['deviceKey'], rString['email'], rString['deviceName'], rString['deviceType'])
        if isDeviceAlreadyRegistere(deviceData):
            response = JsonResponse({"Data":"Device is already registered"})
            response.status_code = 200
            return response
        else:
            if createNewDeviceInfo(deviceData):
                response = JsonResponse({"Data":"Device registered successful"})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({"Data":"Issue while registering new device"})
                response.status_code = 409
                return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def insertingNewValues(request):
    try:
        rString = json.loads(request.body)
        deviceData = deviceInfo(rString['deviceKey'], rString['email'], rString['deviceName'], rString['deviceType'])
        angle = rString['trackingValue']
        if isDeviceAlreadyRegistere(deviceData):
            if insertNewTrackingValue(deviceData, angle):
                response = JsonResponse({"Data":"Tracking value inserted"})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({"Data":"Issue while inserting new tracking value"})
                response.status_code = 409
                return response
        else:
            if createNewDeviceInfo(deviceData):
                if insertNewTrackingValue(deviceData, angle):
                    response = JsonResponse({"Data":"Tracking value inserted"})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Issue while inserting new tracking value"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Issue while registering new device"})
                response.status_code = 409
                return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewAllTheDevices(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1)
        if isValidSessionInformation(sessionInfoObject):
            response = getAllTheDevicesList()
            return response
        else:
            response = JsonResponse({"Data":"You are not authorized to perform the task"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def viewAllTheTrackingInfo(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1)
        if isValidSessionInformation(sessionInfoObject):
            deviceId = rString['deviceId']
            if isValidDevice(deviceId):
                response = getAllTheTrackingDetails(deviceId)
                return response
            else:
                response = JsonResponse({"Data":"Invalid device selected"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"You are not authorized to perform the task"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

def logOutUser(request):
    try:
        basicKey = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(None, basicKey, sessionKey, None, rString['userId'], 1)
        if isValidSessionInformation(sessionInfoObject):
            if loggingOutSession(sessionInfoObject):
                response = JsonResponse({"Data":"User logged out successfully"})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({"Data":"Issue while logging out user"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"You are not authorized to perform the task"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
