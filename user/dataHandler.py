# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Q
from datetime import datetime
from .models import loginMaster, sessionMaster, deviceMaster, trackingMaster

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
file_handler = logging.FileHandler('log/dataHanler.log')
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA VIEWING HOLDER LAYER###################
##############################################################
##############################################################

def isValidLoginRequest(loginRequest):
    #Validating if one and only one record with provide username, password and usertype exist in login master
    loginInfo = loginMaster.objects.values_list().filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password).filter(isActive=1).count()
    if loginInfo==1:
        return True
    else:
        return False

def validSessionBasicAut(basicKey):
    sessionInfo = sessionMaster.objects.values_list().filter(basicAuthenticate__exact=basicKey).count()
    return sessionInfo

def validSessionSessionKey(sessionKey):
    sessionInfo = sessionMaster.objects.values_list().filter(sessionInfo__exact=sessionKey).count()
    return sessionInfo

def getLoginObjectModel(userId):
    loginInfo = loginMaster.objects.get(userId=userId)
    return loginInfo

def getUserIdFromLoginInfo(loginRequest):
    loginInfo = loginMaster.objects.values_list("userId").filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password).filter(isActive=1)
    return loginInfo[0][0]

def isDeviceAlreadyRegistere(deviceData):
    deviceInfo = deviceMaster.objects.values_list().filter(emailAddress__exact=deviceData.email).filter(deviceName__exact=deviceData.deviceName).filter(deviceType__exact=deviceData.deviceType).filter(deviceIdentifier__exact=deviceData.deviceKey).count()
    if deviceInfo==1:
        return True
    else:
        return False

def getDeviceInfo(deviceData):
    deviceInfo = deviceMaster.objects.get(deviceIdentifier__exact=deviceData.deviceKey,emailAddress__exact=deviceData.email, deviceName__exact=deviceData.deviceName, deviceType__exact=deviceData.deviceType)
    return deviceInfo

def isValidSessionInformation(sessionInfo):
    sessionInfoCount = sessionMaster.objects.values_list().filter(basicAuthenticate__exact=sessionInfo.basicKey).filter(sessionInfo__exact=sessionInfo.sessionKey).filter(userId=sessionInfo.userId).filter(isActive=1).count()
    if sessionInfoCount==1:
        return True
    else:
        return False

def userDeviceList():
    deviceInfo = deviceMaster.objects.values_list()
    return deviceInfo

def isValidDevice(deviceId):
    deviceInfo = deviceMaster.objects.values_list().filter(deviceId=deviceId).count()
    if deviceInfo==1:
        return True
    else:
        return False

def userDecviceTracking(deviceId):
    trackingInfo = trackingMaster.objects.values_list().filter(deviceId=deviceId)
    return trackingInfo

#Insert new value in the system
def insertSessionInformation(sessionInfoPassed):
    try:
        sessionInfo = sessionMaster()
        sessionInfo.basicAuthenticate = sessionInfoPassed.basicKey
        sessionInfo.sessionInfo = sessionInfoPassed.sessionKey
        sessionInfo.createdOn = sessionInfoPassed.createdOn
        sessionInfo.userId = getLoginObjectModel(sessionInfoPassed.userId)
        sessionInfo.isActive = sessionInfoPassed.isActive
        sessionInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def createNewDeviceInfo(deviceData):
    try:
        deviceInfo = deviceMaster()
        deviceInfo.emailAddress = deviceData.email
        deviceInfo.deviceIdentifier = deviceData.deviceKey
        deviceInfo.deviceType = deviceData.deviceType
        deviceInfo.deviceName = deviceData.deviceName
        deviceInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertNewTrackingValue(deviceData, angle):
    try:
        trackingInfo = trackingMaster()
        trackingInfo.deviceId = getDeviceInfo(deviceData)
        trackingInfo.DateTimeField = datetime.now()
        trackingInfo.tackingValue = angle
        trackingInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def loggingOutSession(sessionData):
    try:
        sessionInfo = sessionMaster.objects.get(basicAuthenticate__exact=sessionData.basicKey)
        sessionInfo.isActive = 0
        sessionInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False
